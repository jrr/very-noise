package main

import (
	"flag"
	"math/rand"
	"time"

	"github.com/brianvoe/gofakeit/v6"
	"go.uber.org/zap"
)

var (
	errorMessageChancePct = flag.Float64("error-chance", 1, "Percent chance of getting an error log with a stacktrace")
	longMessageChancePct  = flag.Float64("long-chance", 15, "Percent chance of getting a long message")
	minParagraphCount     = flag.Int("min-paragraphs", 1, "Minimum number of paragraphs in a long message")
	maxParagraphCount     = flag.Int("max-paragraphs", 3, "Maximum number of paragraphs in a long message")
	minSentenceCount      = flag.Int("min-sentences", 3, "Minimum number of sentences in a long message")
	maxSentenceCount      = flag.Int("max-sentences", 7, "Maximum number of sentences in a long message")
	minSentenceLength     = flag.Int("min-words", 4, "Minimum number of words in a sentence")
	maxSentenceLength     = flag.Int("max-words", 10, "Maximum number of words in a sentence")
	messageLimit          = flag.Int("messages", 100, "Number of total log messages to print. If set to zero (0), application will not terminate.")
	messagesPerSecond     = flag.Int("message-rate", 5, "Log printing rate (messages per second)")
	slowMode              = flag.Bool("slow-mode", false, "If true, delays will be increased 5x. If variable-rate=true, rates will vary more.")
	variableRate          = flag.Bool("variable-rate", false, "If true, will vary the -message-rate variable within 50% of its set value")
)

var (
	logger *zap.SugaredLogger
)

func init() {
	flag.Parse()
}

func main() {
	messageDelay := time.Duration(1000 / *messagesPerSecond) * time.Millisecond

	if *slowMode {
		messageDelay *= 5
	}

	ticker := time.NewTicker(messageDelay)
	defer ticker.Stop()

	rawLogger, _ := zap.NewProduction(zap.WithCaller(false))
	logger = rawLogger.Sugar()
	defer rawLogger.Sync()

	errorMessageChance := *errorMessageChancePct / 100.0
	longMessageChance := *longMessageChancePct / 100.0

	messageIndex := 0

	for {
		if *messageLimit != 0 && messageIndex >= *messageLimit {
			return
		}
		if *variableRate {
			variedRate := getVariedRate(float64(messageDelay))
			time.Sleep(time.Duration(variedRate))
		}

		select {
		case <-ticker.C:
			messageIndex++
			printLogMessage(errorMessageChance, longMessageChance)
		}
	}

}

func getVariedRate(messageDelay float64) float64 {
	var variedRate float64

	firstDieRoll := rand.Float64()
	secondDieRoll := rand.Float64()

	if firstDieRoll > 0.5 {
		variedRate = messageDelay * (1.0 + secondDieRoll)
	} else {
		variedRate = messageDelay * (1.0 - secondDieRoll)
	}

	if *slowMode {
		variedRate *= 2.5
	}

	return variedRate
}

func printLogMessage(errorMessageChance, longMessageChance float64) {
	var text string

	firstDieRoll := rand.Float64()
	secondDieRoll := rand.Float64()

	if firstDieRoll <= longMessageChance {
		text = gofakeit.Paragraph(
			gofakeit.Number(*minParagraphCount, *maxParagraphCount),
			gofakeit.Number(*minSentenceCount, *maxSentenceCount),
			gofakeit.Number(*minSentenceLength, *maxSentenceLength),
			"")
	} else {
		text = gofakeit.Sentence(gofakeit.Number(*minSentenceLength, *maxSentenceLength))
	}

	if secondDieRoll <= errorMessageChance {
		logger.Errorw(text)
	} else {
		logger.Infow(text, "metric", rand.Float64())
	}
}
