# Very Noise

[![Docker image repo](https://quay.io/repository/jrr/very-noise/status "Docker Repository on Quay")](https://quay.io/repository/jrr/very-noise)

[It's a reference](https://www.youtube.com/watch?v=Osqf4oIK0E8)

Basically this is the noisy neighbor that everyone warned your logging infrastructure about.

Run it in Docker, image is [right here](https://quay.io/jrr/very-noise).

The idea is pretty simple but it's got buttons and knobs to fiddle with.

```
Usage of bin/very-noise:
  -error-chance float
        Percent chance of getting an error log with a stacktrace (default 1)
  -long-chance float
        Percent chance of getting a long message (default 15)
  -max-paragraphs int
        Maximum number of paragraphs in a long message (default 3)
  -max-sentences int
        Maximum number of sentences in a long message (default 7)
  -max-words int
        Maximum number of words in a sentence (default 10)
  -message-rate int
        Log printing rate (messages per second) (default 5)
  -messages int
        Number of total log messages to print. If set to zero (0), application will not terminate. (default 100)
  -min-paragraphs int
        Minimum number of paragraphs in a long message (default 1)
  -min-sentences int
        Minimum number of sentences in a long message (default 3)
  -min-words int
        Minimum number of words in a sentence (default 4)
  -slow-mode
        If true, delays will be increased 5x. If variable-rate=true, rates will vary more.
  -variable-rate
        If true, will vary the -message-rate variable within 50% of its set value
```
