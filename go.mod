module gitlab.com/jrr/very-noise

go 1.16

require (
	github.com/brianvoe/gofakeit/v6 v6.5.0
	go.uber.org/zap v1.18.1
)
